<?php 

$html = "";

$user = 'root';
$password = 'test';
$dbname = 'reserve';
$host = 'localhost:3306';
$dsn = "mysql:host={$host};dbname={$dbname};charset=utf8";

try {
	$pdo = new PDO($dsn, $user, $password);
	$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
	$pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	echo "{$dbname}に接続成功","<br>";

	$sql = "select * from reserve";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	$result = $stm->fetchAll(PDO::FETCH_ASSOC);

	$html .= "<table>";
	$html .= "<thead><tr>";
	$html .= "<th>名前</th>";
	$html .= "<th>予約時間</th>";
	$html .= "<th>終了時間</th>";
	$html .= "</tr></thead>";

	$html .=  "<tbody>";
	foreach ($result as $row){
		$html .= "<tr>";
		$html .= "<td>". $row['id']. "</td>";
		$html .= "<td>". $row['user']. "</td>";
		$html .= "<td>". $row['content']. "</td>";
		$html .= "<td>". $row['date_from']. "</td>";
		$html .= "<td>". $row['date_to']. "</td>";
		$html .= "</tr>";
	}
	$html .= "</tbody>";
	$html .= "</table>";
} catch (Exception $e) {
	echo '<span class="error">お前にはまだ早い、出直して来い</span><br>';
	echo $e->getMessage();
	exit();
}
$pdo = NULL;

?>
<!DOCTYPE html>
<html>
<head>
	<title>予約状況一覧</title>
</head>
<body bgcolor="#c0c0c0">
<center>
<br><br><br><br>
会議室予約状況一覧<br>
<br><br>&emsp;
<input type="button" onclick="location.href='http://localhost/reservation2/resewe.php'"" value="登録" style="width:100px; height:50px;">
<br>
<?php echo $html; ?>
</center>
</body>
</html>